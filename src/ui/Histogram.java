package ui;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Insets;

/**
 * Displays a simple histogram.
 * 
* @version 1.0
 * @author Hans U. Gerber (<a
 * href="mailto:gerber@ifh.ee.ethz.ch">gerber@ifh.ee.ethz.ch</a>)
 */
public class Histogram extends Plot {

    private double[] data;

    public Histogram() {
        setWindow(0.0, 0.0, 1.0, 1.0);
    }

    /**
     * Updates the histogram data and repaints immediately.
     */
    public void setData(double[] data) {
        this.data = data;
        paint(getGraphics());
        //	Why do I call paint() directly and not repaint()?
        //	Because it did not work well with repaint(). It 
        //	looked as if the painting thread was preempted
        //	by higher-priority threads. painting stuttered and
        //	choked.
    }

    public synchronized void paint(Graphics g) {
        //	Draw a 3-D frame:
        Dimension d = size();
        g.setColor(getBackground());
        g.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g.draw3DRect(3, 3, d.width - 7, d.height - 7, false);
        g.clipRect(4, 4, d.width - 8, d.height - 8);
        g.fillRect(4, 4, d.width - 8, d.height - 8);
        setViewport(4, 4, d.width - 4, d.height - 4);
        if (data != null) {
            g.setColor(Color.black);
            double dx = 1.0 / (double) (data.length - 1);
            for (int i = 0; i < data.length; i++) {
                double x = (double) i / (double) (data.length) + dx / 2;
                double y = data[i];
                g.drawLine(xformX(x), xformY(0), xformX(x), xformY(y));
            }
        }
    }
}