package ui;

/**
 * Just a simple data structure.
 * 
* @version 1.0
 * @author Hans U. Gerber (<a
 * href="mailto:gerber@ifh.ee.ethz.ch">gerber@ifh.ee.ethz.ch</a>)
 */
public class RealPoint {

    public RealPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public double x;
    public double y;
}