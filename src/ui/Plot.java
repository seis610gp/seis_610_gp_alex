package ui;

import java.awt.Canvas;

/**
 * This is the base class for simple plots. It offers methods to set the problem
 * coordinate system and the viewport, as well as transformation functions from
 * user coordinates to display units.
 * 
* @version 1.0
 * @author Hans U. Gerber (<a
 * href="mailto:gerber@ifh.ee.ethz.ch">gerber@ifh.ee.ethz.ch</a>)
 */
public class Plot extends Canvas {

    protected double xMin = -0.05;
    protected double yMin = -1.05;
    protected double xMax = +1.05;
    protected double yMax = +1.05;
    private int vLeft = 0;
    private int vTop = 0;
    private int vRight = 100;
    private int vBottom = 100;

    public void setWindow(double xMin, double yMin, double xMax, double yMax) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
    }

    public void setViewport(int vLeft, int vTop, int vRight, int vBottom) {
        this.vLeft = vLeft;
        this.vTop = vTop;
        this.vRight = vRight;
        this.vBottom = vBottom;
    }

    public int xformX(double x) {
        return (int) ((x - xMin) / (xMax - xMin) * (double) (vRight - vLeft)) + vLeft;
    }

    public int xformY(double y) {
        return (int) ((y - yMin) / (yMax - yMin) * (double) (vTop - vBottom)) + vBottom;
    }
}