
import java.applet.*;
import ui.*;
import java.awt.Button;
import java.awt.Event;
import java.awt.TextArea;
import java.awt.Label;
import java.awt.Panel;
import java.awt.BorderLayout;
import gp.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observer;
import java.util.Observable;

/* 
 Symbolic regression with genetic programming. Hoorah!!!!
 */
public class Approx extends Applet implements Observer {

    //		m_fStandAlone will be set to true if applet is run standalone
    boolean m_fStandAlone = false;
    ArrayList<Integer> genList = new ArrayList<>();
    ArrayList<Double> fitnessList = new ArrayList<>();
    Button startButton;
    Button pauseButton;
    Button stopButton;
    Button settingsButton;
    XYPlot plot;
    Histogram histogram;
    TextArea status;
    TextArea results;
    GP gp;

    public static void main(String args[]) {
        // Create Toplevel Window to contain applet Approx
        ApproxFrame frame = new ApproxFrame("Approx");

        // Must show Frame before we size it so insets() will return valid values
        frame.show();
        frame.hide();

        // The following code starts the applet running within the frame window.
        // It also calls GetParameters() to retrieve parameter values from the
        // command line, and sets m_fStandAlone to true to prevent init() from
        // trying to get them from the HTML page.
        Approx applet_Approx = new Approx();
        frame.add("Center", applet_Approx);
        applet_Approx.m_fStandAlone = true;
        applet_Approx.init();
        applet_Approx.start();
        frame.show();
        frame.resize(frame.insets().left + frame.insets().right + 500,
                frame.insets().top + frame.insets().bottom + 400);

        System.out.println();
    }

    public Approx() {
    }

    public String getAppletInfo() {
        return "Name: Approx\r\n"
                + "Author: Hans U. Gerber\r\n"
                + "Created with Microsoft Visual J++ Version 1.0";
    }

    /**
     * The init() method is called by the AWT when an applet is first loaded or
     * reloaded.
     */
    public void init() {
        Panel buttonPanel = new Panel();
        buttonPanel.setLayout(new StackLayout(StackLayout.VERTICAL));
        buttonPanel.add("Wide", new Panel());
        startButton = new Button("Start");
        buttonPanel.add("Wide", startButton);
        pauseButton = new Button("Pause");
        buttonPanel.add("Wide", pauseButton);
        stopButton = new Button("Stop");
        buttonPanel.add("Wide", stopButton);
        buttonPanel.add("Wide", new Panel());
        settingsButton = new Button("Settings...");
        buttonPanel.add("Wide", settingsButton);

        Panel plotPanel = new Panel();
        plotPanel.setLayout(new BorderLayout());
        plotPanel.add("North", new Label("Approximation"));
        plot = new XYPlot();
        plotPanel.add("Center", plot);

        Panel histoPanel = new Panel();
        histoPanel.setLayout(new BorderLayout());
        histoPanel.add("North", new Label("Fitnesses"));
        histogram = new Histogram();
        histoPanel.add("Center", histogram);

        Panel resultsPanel = new Panel();
        resultsPanel.setLayout(new BorderLayout());
        resultsPanel.add("North", new Label("Results"));
        results = new TextArea();
        resultsPanel.add("Center", results);

        Panel statusPanel = new Panel();
        statusPanel.setLayout(new BorderLayout());
        statusPanel.add("North", new Label("Status"));
        status = new TextArea();
        statusPanel.add("Center", status);

        Panel outputPanel = new Panel();
        FractionalLayout outputLayout = new FractionalLayout();
        outputPanel.setLayout(outputLayout);
        outputLayout.setConstraint(plotPanel, new FrameConstraint(
                0.0, 4,
                0.0, 0,
                0.5, -4,
                0.5, 0));
        outputPanel.add(plotPanel);
        outputLayout.setConstraint(histoPanel, new FrameConstraint(
                0.0, 4,
                0.5, 0,
                0.5, -4,
                1.0, 0));
        outputPanel.add(histoPanel);
        outputLayout.setConstraint(statusPanel, new FrameConstraint(
                0.5, 4,
                0.0, 0,
                1.0, 0,
                0.2, 0));
        outputPanel.add(statusPanel);
        outputLayout.setConstraint(resultsPanel, new FrameConstraint(
                0.5, 4,
                0.2, 0,
                1.0, 0,
                1.0, 0));
        outputPanel.add(resultsPanel);

        setLayout(new StackLayout(StackLayout.HORIZONTAL, 8));
        add("Wide Tall Flush", outputPanel);
        add("Top", buttonPanel);

        startButton.enable(false);
        pauseButton.enable(false);
        stopButton.enable(false);
        settingsButton.enable(false);
        gp = new GP();
        gp.addObserver(this);
        startButton.enable(true);
        settingsButton.enable(true);
        gp.init();
    }

    /**
     * destroy() is called when the applet is terminating and being unloaded.
     */
    public void destroy() {
        gp.stop();
    }

    /**
     * The start() method is called when the page containing the applet appears
     * in the browser. If the user moves to another page while the GP algorithm
     * is running, the GP thread is suspended. When returning to the page with
     * our applet, the GP thread resumes.
     */
    public void start() {
        gp.thaw();
    }

    /**
     * The stop() method is called when the page containing the applet is no
     * longer on the screen. If the user moves to another page while the GP
     * algorithm is running, the GP thread is suspended. When returning to the
     * page with our applet, the GP thread resumes.
     */
    public void stop() {
        gp.freeze();
    }

    /**
     * action() is called whenever one of the command buttons is pressed. The
     * button that was pressed is disabled immediately to avoid races. Only when
     * the GP notifies the applet of a state change (via update()), the
     * appropriate	buttons are re-enabled.
     */
    public synchronized boolean action(Event evt, Object what) {
        if (evt.target == startButton) {
            startButton.enable(false);
            settingsButton.enable(false);
            gp.start();
            return true;
        }
        if (evt.target == pauseButton) {
            pauseButton.enable(false);
            int state = gp.getState();
            switch (state) {
                case GP.STARTED:
                case GP.RESUMED:
                    gp.suspend();
                    break;
                case GP.SUSPENDED:
                    gp.resume();
                    break;
                default:
                    break;
            }
            return true;
        }
        if (evt.target == stopButton) {
            stopButton.enable(false);
            pauseButton.enable(false);
            gp.stop();
            return true;
        }
        if (evt.target == settingsButton) {
            settingsButton.enable(false);
            startButton.enable(false);
            Dialog d = new SettingsDialog(Dialog.findFirstFrame(this), gp);
            d.show();
            settingsButton.enable(true);
            startButton.enable(true);
            return true;
        }
        return false;
    }

    /**
     * Is called whenever the GP model changes. The views on the model -- which
     * are contained here in the applet -- are updated accordingly.
     */
    public void update(Observable o, Object arg) {
        if (arg instanceof GPMessageStateChanged) {
            GPMessageStateChanged msg = (GPMessageStateChanged) arg;
            switch (msg.newState) {
                case GP.STARTED:
                    startButton.enable(false);
                    pauseButton.enable(true);
                    stopButton.enable(true);
                    settingsButton.enable(false);
                    plot.setTrace(1, null);
                    plot.setTrace(2, null);
                    results.setText("");
                    histogram.setData(null);
                    status.setText("Starting up...");
                    break;
                case GP.SUSPENDED:
                    startButton.enable(false);
                    pauseButton.enable(true);
                    pauseButton.setLabel("Resume");
                    stopButton.enable(true);
                    settingsButton.enable(false);
                    status.setText("Paused");
                    break;
                case GP.RESUMED:
                    startButton.enable(false);
                    pauseButton.enable(true);
                    pauseButton.setLabel("Pause");
                    stopButton.enable(true);
                    settingsButton.enable(false);
                    status.setText("Resuming...");
                    break;
                case GP.STOPPED:
                    startButton.enable(true);
                    pauseButton.enable(false);
                    pauseButton.setLabel("Pause");
                    stopButton.enable(false);
                    settingsButton.enable(true);
                    status.setText("Stopped\n" + msg.text);
                    break;
            }
        }
        if (arg instanceof GPMessageFitnessCasesSet) {
            GPMessageFitnessCasesSet msg = (GPMessageFitnessCasesSet) arg;
            plot.setTrace(0, msg.data);
        }
        if (arg instanceof GPMessageEvaluatingIndividual) {
            GPMessageEvaluatingIndividual msg = (GPMessageEvaluatingIndividual) arg;
            status.setText("Evaluating individual #" + msg.individualNr + "\n"
                    + "of generation " + msg.generationNr);
            plot.setTrace(1, msg.data);
        }
        if (arg instanceof GPMessageBestFound) {
            GPMessageBestFound msg = (GPMessageBestFound) arg;
            plot.setTrace(2, msg.data);
            results.setText("The best individual so far\n"
                    + "was found in generation "
                    + msg.generation + ".\n"
                    + "Its adjusted fitness is " + msg.fitness + "\n\n"
                    + "Its function is:\n\n"
                    + msg.program);
            System.out.println("\n\n\n");
            System.out.println("Output function:" + msg.program);
            System.out.println("The best individual was found in generation:" + msg.generation);
            System.out.println("Fitness:" + msg.fitness);
            
            fitnessList.add(new Double(msg.fitness));
            System.out.println(fitnessList);
            genList.add(new Integer(msg.generation));
            System.out.println(genList);

            System.out.println("\n\n\n");
        }
        if (arg instanceof GPMessageEvaluatingPopulation) {
            GPMessageEvaluatingPopulation msg = (GPMessageEvaluatingPopulation) arg;
            plot.setTrace(1, null);
            histogram.setData(msg.fitness);
        }
    }
}
