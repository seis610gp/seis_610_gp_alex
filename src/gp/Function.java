/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gp;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

/**
 *
 * @author gelb1167
 */
public class Function {
        
    public static void main(String[] args) {
        String s=readFile("U:\\GP\\src\\gp\\outputFunction.txt");
    }

    public static boolean isParenthesisMatch(String str) {
        if (str.charAt(0) == '{') {
            return false;
        }

        Stack<Character> stack = new Stack<Character>();

        char c;
        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);

            if (c == '(') {
                stack.push(c);
            } else if (c == '{') {
                stack.push(c);
            } else if (c == ')') {
                if (stack.empty()) {
                    return false;
                } else if (stack.peek() == '(') {
                    stack.pop();
                } else {
                    return false;
                }
            } else if (c == '}') {
                if (stack.empty()) {
                    return false;
                } else if (stack.peek() == '{') {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.empty();
    }

    public String readFile(String filename) {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
