package gp;

/**
 * This is the base class for all the messages that a GP object (an Observable)
 * can send to its observers.
 */
public abstract class GPMessage {
}