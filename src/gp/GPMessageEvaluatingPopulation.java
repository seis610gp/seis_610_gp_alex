package gp;

/**
 * A GP object sends this message to its observers whenever the genetic
 * algorithm has evaluated the fitnesses of all individuals in the population.
 */
public class GPMessageEvaluatingPopulation extends GPMessage {

    public int generation;
    public double[] fitness;

    GPMessageEvaluatingPopulation(int generation, double[] fitness) {
        this.generation = generation;
        this.fitness = fitness;
    }
}