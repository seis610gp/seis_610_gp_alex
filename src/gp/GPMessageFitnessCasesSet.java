package gp;

import ui.RealPoint;

/**
 * GP object sends this message to its observers whenever the fitness cases
 * change. This happens for the first time when the GP creates some initial
 * default fitness cases. The same message is sent whenever the user edits the
 * fitness cases.
 */
public class GPMessageFitnessCasesSet extends GPMessage {

    public RealPoint[] data;

    GPMessageFitnessCasesSet(RealPoint[] data) {
        this.data = data;
    }
}