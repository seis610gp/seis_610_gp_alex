package gp;

import ui.RealPoint;

/**
 * A GP object sends this message to its observers whenever the genetic
 * algorithm has evaluated the fitness of an individual.
 */
public class GPMessageEvaluatingIndividual extends GPMessage {

    public int generationNr;
    public int individualNr;
    public RealPoint[] data;

    GPMessageEvaluatingIndividual(int generationNr, int individualNr, RealPoint[] data) {
        this.generationNr = generationNr;
        this.individualNr = individualNr;
        this.data = data;
    }
}