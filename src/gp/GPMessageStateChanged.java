package gp;

/**
 * GP object sends this message to its observers whenever the execution state of
 * the genetic algorithm changes, such as from "Running" to "Stopped".
 */
public class GPMessageStateChanged extends GPMessage {

    public int newState;
    public String text;

    GPMessageStateChanged(int newState, String text) {
        this.newState = newState;
        this.text = text;
    }
}