package gp;

import ui.RealPoint;

/**
 * A GP object sends this message to its observers whenever the genetic
 * algorithm has found a new best individual.
 */
public class GPMessageBestFound extends GPMessage {

    public int generation;
    public String program;
    public RealPoint[] data;
    public double fitness;

    GPMessageBestFound(int generation, String program, RealPoint[] data, double fitness) {
        this.generation = generation;
        this.program = program;
        this.data = data;
        this.fitness = fitness;
    }
}